package com.hash.helloweb;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WebController {
    @RequestMapping("/hello")
    public @ResponseBody String helloWeb() {
        return "Hello World!";
    }
}
